package com.company;

public class Main {

    public static void drawFigure(Figure figure, Integer size) {
        figure.print(size);
    }

    public static void main(String[] args) {

        Figure square = new Square();
        Figure triangle = new Triangle();
        Figure circle = new Circle();

        drawFigure(square, 5);
        drawFigure(triangle, 5);
        drawFigure(circle, 4);
    }
}

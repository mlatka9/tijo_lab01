package com.company;

public class Circle extends Figure{
    public void print(Integer size){
        for(int i = 0; i< size; i++ ) {
            for(int j = 0; j< size; j++ ) {
                if(i == 0 || i == size - 1 || j == 0 || j == size - 1) {
                    System.out.print("O ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }

    }
}

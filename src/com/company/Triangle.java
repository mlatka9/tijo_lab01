package com.company;

public class Triangle extends Figure {
    public void print(Integer size){
        for(int i = 0; i< size; i++ ) {
            for(int j = 0; j< size; j++ ) {
                if(i + 1 > j) {
                    System.out.print("O");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }
}
